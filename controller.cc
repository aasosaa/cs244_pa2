#include <iostream>

#include "controller.hh"
#include "timestamp.hh"

using namespace std;

#define AIMD_divisor 2.0
#define AIMD_multiplier  1.75
#define SPEEDY_INCREASE_CONST 1.0
#define QUICKNESS_THRESH_COUNT 5
#define SLOWNESS_THRESH_COUNT 5
#define QUICKNESS_CONST 1.25
#define SLOWNESS_CONST 1.85
#define ACK_TIMEOUT 5
#define ACK_THRESH_SPEEDY_INCREASE 5
#define MIN_WINDOW 2.0

float cur_window = 15.0;
uint64_t rtt_estimation = 100.0;
uint64_t min_rtt = 100.0;

uint32_t acks_since_timeout = 0;
uint32_t acks_since_speedy_increase = 0;
uint32_t num_consec_quick = 0;
uint32_t num_consec_slow = 0;


/* Default constructor */
Controller::Controller( const bool debug )
  : debug_( debug )
{}

void Controller::inform_of_timeout(bool severe_timeout,uint64_t timeout_rtt)
{
  /* If this timeout was 'severe', cut the window by AIMD_divisor. */
  if (severe_timeout || (timeout_rtt > 3 * min_rtt))
    cur_window = cur_window / AIMD_divisor;
  
  /* Otherwise, cut the window by 2/3 * AIMD_divisor. */ 
  else
    cur_window = cur_window / (AIMD_divisor / 1.5);

  /* Enforce a minimum. */
  if (cur_window < MIN_WINDOW)
    cur_window = MIN_WINDOW;

  /* Reset min_rtt to some reasonable number. */
  min_rtt = 0.8 * min_rtt + 0.2 * rtt_estimation;
}
/* Get current window size, in datagrams */
unsigned int Controller::window_size( void )
{
  /* Default: fixed window size of 100 outstanding datagrams */
  unsigned int the_window_size = (unsigned int) cur_window;

  if ( debug_ ) {
    cerr << "At time " << timestamp_ms()
	 << " window size is " << the_window_size << endl;
  }

  return the_window_size;
}

/* A datagram was sent */
void Controller::datagram_was_sent( const uint64_t sequence_number,
				    /* of the sent datagram */
				    const uint64_t send_timestamp )
                                    /* in milliseconds */
{
  /* Default: take no action */

  if ( debug_ ) {
    cerr << "At time " << send_timestamp
	 << " sent datagram " << sequence_number << endl;
  }
}

/* An ack was received */
bool Controller::ack_received( const uint64_t sequence_number_acked,
			       /* what sequence number was acknowledged */
			       const uint64_t send_timestamp_acked,
			       /* when the acknowledged datagram was sent (sender's clock) */
			       const uint64_t recv_timestamp_acked,
			       /* when the acknowledged datagram was received (receiver's clock)*/
			       const uint64_t timestamp_ack_received )
                               /* when the ack was received (by sender) */
{
  /* Default: take no action */
  uint64_t rtt_new = timestamp_ack_received - send_timestamp_acked;

  /* Update rtt moving average and thresholds */
  rtt_estimation = 0.9 * rtt_estimation + 0.1 * rtt_new;

  float quickness_thresh = min_rtt * QUICKNESS_CONST;
  float slowness_thresh = min_rtt * SLOWNESS_CONST;

  bool rapid_send = false;

  /* If this RTT was quick, update our quick count. If not, reset it. */
  if (rtt_new <= quickness_thresh)
     num_consec_quick++;
  else 
     num_consec_quick = 0;

  /* Likewise, update the slowness count if necessary. */
  if (rtt_new >= slowness_thresh)
     num_consec_slow++;
  else
    num_consec_slow = 0;

  
  /* If we've been going fast for a while, increase window. Also, tell the sender to
     send the next datagram immediately. */
  if (acks_since_speedy_increase > ACK_THRESH_SPEEDY_INCREASE &&
      num_consec_quick >= QUICKNESS_THRESH_COUNT) {
    cur_window += SPEEDY_INCREASE_CONST;
    acks_since_speedy_increase = 0;
    rapid_send = true;
  }
  
  /* If we've been going slow for a while and are suddenly going faster, inflate the window. 
     Also, tell the sender to send the next datagram immediately. */
  else if (num_consec_slow >= SLOWNESS_THRESH_COUNT && rtt_new <= quickness_thresh) {
    cur_window += cur_window; 
    rapid_send = true;
  } 
  
  /* Our RTT is lower than our quickness thresh - inflate the window fast. */
  else if (rtt_new <= quickness_thresh) {
    cur_window += 2.2 * AIMD_multiplier / cur_window;
    acks_since_speedy_increase++;
    rapid_send = true;
  }

  /* Standard AIMD increase if our RTT new is beneath 1.5*min_rtt. */
  else if (rtt_new <= (slowness_thresh + quickness_thresh) / 2) { 
    cur_window += AIMD_multiplier/cur_window;
    acks_since_speedy_increase++;
  }
  
  /* If our rtt is slow; just increment the acks and slowly bring down the cong. window. */
  else if (rtt_new <= slowness_thresh) {    
    cur_window -= 1.0 / cur_window; 
    acks_since_speedy_increase++;
  }

  /* If we've had many acks since the last timeout and we've slowed down, inform of an 
     impending timeout. */
  if (acks_since_timeout > ACK_TIMEOUT && rtt_new > slowness_thresh) {
    inform_of_timeout(false, rtt_new);
    acks_since_timeout = 0;
  }

  /* Otherwise, we update the number of acks since timeout. */
  else 
    acks_since_timeout++;

  /* Update min_rtt if necessary. */
  if (rtt_new < min_rtt) {
    min_rtt = rtt_new;
  }

  /* Cap the window if necessary. */
  if (cur_window < MIN_WINDOW)
    cur_window = MIN_WINDOW;
  
  if ( debug_ ) {
    cerr << "At time " << timestamp_ack_received
	 << " received ack for datagram " << sequence_number_acked
	 << " (send @ time " << send_timestamp_acked
	 << ", received @ time " << recv_timestamp_acked << " by receiver's clock)"
	 << endl;
  }
  return rapid_send;
}

/* How long to wait (in milliseconds) if there are no acks
   before sending one more datagram */
unsigned int Controller::timeout_ms( void )
{
  /* We set this to some "reasonable value" - it's reasonable to assume that if
     we see no packets for 1.5 * the minRTT, we probably have timed out somewhere. */
  return 1.5 * min_rtt;
}
